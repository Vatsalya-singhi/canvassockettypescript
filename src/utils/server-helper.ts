import { User } from "../model/user.model";

// dictonary of room : array of users { roomName : User[] }
var rooms: any = {};

// user creates room
export function userCreate(id: string, username: string, room: string, password: string, role: string, editCan: boolean, authID: string, photoURL: string): User {
    const user: User = new User({ id, username, room, password, role, editCan, authID, photoURL });
    if (rooms[room] != null) return null;

    let userList: any[] = [];
    userList.push(user);
    rooms[room] = userList;
    return user;
}

// Join user to chat
export function userJoin(id: string, username: string, room: string, password: string, role: string, authID: string, photoURL: string): any {
    let joinee: User = new User({ id, username, room, password, role, authID, photoURL });

    if (rooms[joinee.room] == null) return { error: "Room doesn't exists" };

    let roomUsers: User[] = rooms[joinee.room];

    if (roomUsers && roomUsers[0]) {
        if (roomUsers[0].password != password) {
            return { error: "Invalid Password" };
        }

        // check if list has authID already
        let flag: number = roomUsers.findIndex((user: User) => user.authID == joinee.authID);
        if (flag != -1) {
            return { error: "User has already joined" };
        }

        let creatorIndex = roomUsers.findIndex((user: User) => user.role == "creator");
        if (creatorIndex == -1) {
            roomUsers[0].role = "creator";
            roomUsers[0].editCan = true;
            rooms[room] = roomUsers;
        }
        // finally get creatorIndex
        creatorIndex = roomUsers.findIndex((user: User) => user.role == "creator");

        joinee.editCan = roomUsers[creatorIndex].editCan;

        roomUsers.push(joinee);
        rooms[room] = roomUsers;

        return joinee;
    } else {
        return { error: "Room has no users" };
    }
}

// Get current user
export function getCurrentUser(id: string, room: string, authID: string): User {
    return rooms && rooms[room] ? (rooms[room] as User[]).find((user: User) => user.authID == authID) : null;
}

// User leaves chat
export function userLeave(id: string): any {
    let keyVal: string;
    let userIndex: number;

    let userFound: boolean = Object.keys(rooms).some((key: string) => {
        let userList: User[] = rooms[key];
        keyVal = key;
        let indee = userList.findIndex((user: any, index: number) => {
            userIndex = index;
            return user.id == id;
        });

        return indee == -1 ? false : true;
    })

    if (!userFound) return {};
    if (keyVal == null) return {};
    if (userIndex == null) return {};

    let userList: User[] = rooms[keyVal];
    let leftUser: User = userList.splice(userIndex, 1)[0];

    if (leftUser.role == "creator") {
        let index: number = userList.findIndex(user => user.role == "creator");
        if (index == -1) {
            // no one in group has edit access or is a creator
            // check if any member exist and give permission
            if (userList && userList[0]) {
                userList[0].editCan = leftUser.editCan;
                userList[0].role = "creator";
            }

        }
    }

    //update userlist
    rooms[keyVal] = userList;

    return {
        leftUser: leftUser,
        deleteRoom: userList.length == 0 ? true : false,
        room: keyVal,
    };

}

// Get room users
export function getRoomUsers(room: string): User[] {
    let roomUsers: User[] = rooms[room];
    return roomUsers;
    // .map((user: any) => {
    //     return {
    //         id: user.id,
    //         username: user.username,
    //         role: user.role,
    //         editCan: user.editCan,
    //         authID: user.authID,
    //     }
    // })
}

export function sendAuthorOrJoineeRequest(room: string): string {
    let list: User[] = getRoomUsers(room);
    let creatorIndex: number = list.findIndex((data: User) => data.role == "creator");
    if (creatorIndex != -1) {
        return list[creatorIndex].id;
    }

    let joineeIndex: number = list.findIndex((data: User) => data.role == "joinee");
    if (joineeIndex != -1) {
        return list[joineeIndex].id;
    }

    return null;
}

export function removeRoom(room: string): void {
    delete rooms[room];
}

export function updateUserConfig(roomName: string, id: string, editCan: boolean): User {
    let userList: User[] = rooms[roomName];

    let userIndex: number = userList.findIndex((user: User) => {
        return user.id == id;
    });

    if (userIndex != -1) {
        userList[userIndex].editCan = editCan;
        rooms[roomName] = userList;
        return userList[userIndex];
    }
    return null;
}