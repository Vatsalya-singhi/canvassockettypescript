import { Server } from "./server";

var server = new Server();

server.listen((port: any) => {
    console.log(`Server is listening on ${port}`);
});

server.handleSocketConnection();

// pushing code to heroku 
// git add . && git commit -am "update" && git push heroku branch:master && heroku logs --tail
// git add . && git commit -am "update" && git push heroku videoapp:master && heroku logs --tail
// git add . && git commit -am "update" && git push heroku sockets:master && heroku logs --tail
// git push heroku <currentgitbranchName>:master


// git add . && git commit -m "npm run start script update" && git push origin sockets
// git push heroku sockets:master && heroku logs --tail


// "start": "./node_modules/.bin/tsc --project tsconfig.json && node build/index",
// "starttest1": "./node_modules/.bin/tsc --module commonjs --outDir build/ --noImplicitAny --sourceMap --target ES6 src/index.ts && node build/index.js"
// have swapped start with starttest1 in package.json file

// heroku deploy steps
// 1. heroku npm installs based on env
// 2. postinstall script is called which starts tsc and transcompiles TS to JS files under build folder
// 3. procfile calls start script which starts the server and listens to requests
// 4. deploy script can be used to push code to heroku
// 5. Note have typescript in dev dependencies to use tsc as command else it will throw error


// package.json scripts and main
// "main": "src/index.ts",
// "scripts": {
//     "start": "node src/index", // doesn't work for some BS reason
//     "dev": "nodemon src/index", // works
//     "postinstall": "npm run build"
//   },

// clear node module cache
// heroku config:set NODEMODULESCACHE=false && heroku config:unset NODEMODULESCACHE