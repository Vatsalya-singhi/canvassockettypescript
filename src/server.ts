import { Application } from "express";
import { Server as SocketIOServer } from "socket.io";
import { createServer, Server as HTTPServer } from "http";
import { userCreate, getRoomUsers, userJoin, sendAuthorOrJoineeRequest, getCurrentUser, updateUserConfig, userLeave, removeRoom } from './utils/server-helper';
import { User } from "./model/user.model";


const http = require('http');
const cors = require('cors');
const express = require('express');
const socketio = require('socket.io');

export class Server {

    public httpServer: HTTPServer;
    public app: Application;
    public io: SocketIOServer;
    public PORT: any = process.env.PORT || 3000; // uncomment when pushing to heroku
    // public PORT: any = 3000; // uncomment when using local

    constructor() {
        this.initialize();
        this.handleSocketConnection();
    }

    public initialize(): void {
        this.app = express();
        
        // default working code
        // this.httpServer = createServer(this.app);
        // this.io = socketio(this.httpServer);

        // new cors implementation
        this.httpServer = createServer(this.app);
        this.io = socketio(this.httpServer, {
            cors: {
              origin: '*', //"http://localhost:8100",
              methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH'],
            //   credentials: true,
            //   transports : ['websocket','polling', 'flashsocket'],
            },
            // transports : ['websocket','polling', 'flashsocket'],
        });
    }

    public listen(callback: (port: number) => void): void {
        this.httpServer.listen(this.PORT, () =>
            callback(this.PORT)
        );
    }

    public handleSocketConnection(): void {

        this.io.on("connection", (socket: any) => {
            
            socket.removeAllListeners();
            socket.removeAllListeners();

            console.log('new WS at=>', socket.id);
            // CREATE-ROOM
            socket.on('create-room', ({ username, room, password, role, editCan, authID, photoURL }: any) => {
                return this.createRoom(socket, username, room, password, role, editCan, authID, photoURL);
            })

            // JOIN ROOM
            socket.on('join-room', ({ username, room, password, role, authID, photoURL }: any) => {
                return this.joinRoom(socket, username, room, password, role, authID, photoURL);
            })

            // Listen for sent-points
            socket.on('send-points', ({ action, points, room, authID }: any) => {
                return this.sendPoints(socket, action, points, room, authID);
            })

            // Send special-points to new joinee
            socket.on('special-points', ({ action, points, to, room, authID }: any) => {
                return this.specialPoints(socket, action, points, to, room, authID);
            })

            // Send Config to specific users
            socket.on('update-config', ({ roomName, id, editCan }: any) => {
                this.updateConfig(socket, roomName, id, editCan);
            })

            
            // same but calling it manually
            socket.on('leave-room', () => {
                console.log('leave room called');
                return this.disconnect(socket);
            })
            
            // Runs when client disconnects
            socket.on('disconnect', () => {
                return this.disconnect(socket);
            })
            
            /**
            *  Video call functions
            */

            // socket.on("call-user", (data: any) => {
            //     socket.to(data.to).emit("call-made", {
            //         offer: data.offer,
            //         socket: socket.id
            //     });
            // });

            // socket.on("make-answer", (data: any) => {
            //     socket.to(data.to).emit("answer-made", {
            //         socket: socket.id,
            //         answer: data.answer
            //     });
            // });

            // CHECKING THIS NOW

            socket.on("broadcast", ({ room }: any) => {
                console.log('on broadcast');
                this.io.to(room).emit("broadcast", {
                    id: socket.id,
                });
            });

            socket.on("watcher", ({ id }: any) => {
                console.log('on watcher');
                socket.to(id).emit("watcher", { // emit to newly joined ID
                    id: socket.id //aka existing room member ID and not newly joined ID
                });
            });

            socket.on("offer", ({ id, offer }: any) => {
                console.log('on offer');
                socket.to(id).emit("offer", {
                    id: socket.id,
                    offer: offer,
                });
            });

            socket.on("answer", ({ id, answer }: any) => {
                console.log('on answer');
                socket.to(id).emit("answer", {
                    id: socket.id,
                    answer: answer,
                });
            });

            socket.on("candidate", ({ id, candidate }: any) => {
                console.log('on candidate');
                socket.to(id).emit("candidate", {
                    id: socket.id,
                    candidate: candidate
                });
            });

        });
    }



    /**
    * Secondary Functions
    */

    public createRoom(socket: any, username: string, room: string, password: string, role: string, editCan: boolean, authID: string, photoURL: string) {

        // check if room with same name exist
        var user: User = userCreate(socket.id, username, room, password, role, editCan, authID, photoURL);

        if (user != null) {
            socket.join(user.room);
            // Welcome to current user
            socket.emit('message', {
                type: 'welcome-message',
                message: `Welcome to ${user.room}`,
            });
            socket.emit('create-room-callback', {
                type: "success",
                message: `Welcome to ${user.room}`,
                user: user
            });

            // Send users and room info
            setTimeout(() => {
                this.io.to(user.room).emit('room-users', {
                    room: user.room,
                    users: getRoomUsers(user.room)
                });
            }, 5000);

        } else {
            // room with same name exist send error
            socket.emit('create-room-callback', {
                type: "error",
                message: `Room with similar name already exist..`,
                user: user
            });
        }
    }

    public joinRoom(socket: any, username: string, room: string, password: string, role: string, authID: string, photoURL: string) {
        var user: any = userJoin(socket.id, username, room, password, role, authID, photoURL);

        if (user && user.error) {
            // Send error to user
            socket.emit('join-room-callback', {
                type: "error",
                message: user.error,
                user: user
            });
            return;
        }

        socket.join(user.room);

        // Welcome to current user
        socket.emit('join-room-callback', {
            type: "success",
            message: `Welcome to ${user.room}`,
            editCan: user.editCan,
            user: user
        });

        // ask creator to emit points
        let userSocketID = sendAuthorOrJoineeRequest(user.room);
        if (userSocketID && userSocketID != null) {
            this.io.to(userSocketID).emit('specialCall', {
                type: "send datapoints",
                to: user.id,
                room: user.room
            });
        }

        // BroadCast when a user connects
        socket.broadcast.to(user.room).emit('message',
            {
                type: 'user-joined-message',
                message: `${user.username} has joined the chat..`,
                id: user.id,
            });

        // Send users and room info
        this.io.to(user.room).emit('room-users', {
            room: user.room,
            users: getRoomUsers(user.room)
        });
    }

    public sendPoints(socket: any, action: any, points: any, room: string, authID: string) {
        let user: User = getCurrentUser(socket.id, room, authID);

        if (!user || user == null) return;
        console.log('user made edit =>', user);
        if (user.editCan == true || user.role == "creator") {
            this.io.to(user.room).emit('fetch-points', {
                action: action,
                points: points,
                createdAt: new Date(),
            });

        } else {
            console.log(`This user ${user.id} cannot send points due to access restrictions`);
        }
    }

    public specialPoints(socket: any, action: any, points: any, to: string, room: string, authID: string) {
        var user: User = getCurrentUser(socket.id, room, authID);

        if (!user || user == null) return;

        setTimeout(() => {
            this.io.to(to).emit('fetch-points', {
                action: action,
                points: points,
                createdAt: new Date(),
            });

            // Send users and room info
            this.io.to(room).emit('room-users', {
                room: room,
                users: getRoomUsers(user.room)
            });
        }, 5000);
    }

    public updateConfig(socket: any, roomName: string, id: string, editCan: boolean) {
        // update that room
        let user: User = updateUserConfig(roomName, id, editCan);
        if (user) {
            this.io.to(user.id).emit('config', {
                ...user,
            })

            // Send users and room info
            this.io.to(roomName).emit('room-users', {
                room: user.room,
                users: getRoomUsers(user.room)
            });

            socket.emit('update-config-callback', {
                type: "success",
                message: `Configuration updated`,
            })

            return;
        }

        socket.emit('update-config-callback', {
            type: "fail",
            message: `Configuration not updated`,
        })

        return;
    }

    public disconnect(socket: any) {
        console.log('disconnect=>', socket.id);
        var { leftUser, deleteRoom, room } = userLeave(socket.id);
        console.log('on disconnect =>', leftUser, deleteRoom, room);
        if (leftUser) {
            // Send users and room info
            this.io.to(leftUser.room).emit('message', {
                type: 'user-left-message',
                message: `${leftUser.username} has left the chat`,
                id: leftUser.id,
            });
            // to remove stream
            this.io.to(leftUser.room).emit("disconnectPeer", socket.id);
            this.io.to(leftUser.room).emit('room-users', {
                room: leftUser.room,
                users: getRoomUsers(leftUser.room)
            });

            console.log('deleteRoom=>', deleteRoom);
            if (deleteRoom) {
                socket.leave(room);
                removeRoom(room);
            }
        }
    }

}