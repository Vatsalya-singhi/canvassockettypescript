export class User {
    public id: string;
    public username: string;
    public room: string;
    public password: string;
    public role: string;
    public editCan: boolean;
    public authID: string;
    public photoURL: string;

    constructor(obj: any = {}) {
        this.id = obj.id ? obj.id : "";
        this.username = obj.username ? obj.username : "";
        this.room = obj.room ? obj.room : "";
        this.password = obj.password ? obj.password : "";
        this.role = obj.role ? obj.role : "joinee";
        this.editCan = obj.editCan ? obj.editCan : false;
        this.authID = obj.authID ? obj.authID : "";
        this.photoURL = obj.photoURL ? obj.photoURL : "";
    }
}