"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const http_1 = require("http");
const server_helper_1 = require("./utils/server-helper");
const http = require('http');
const cors = require('cors');
const express = require('express');
const socketio = require('socket.io');
class Server {
    // public PORT: any = 3000; // uncomment when using local
    constructor() {
        this.PORT = process.env.PORT || 3000; // uncomment when pushing to heroku
        this.initialize();
        this.handleSocketConnection();
    }
    initialize() {
        this.app = express();
        // default working code
        // this.httpServer = createServer(this.app);
        // this.io = socketio(this.httpServer);
        // new cors implementation
        this.httpServer = (0, http_1.createServer)(this.app);
        this.io = socketio(this.httpServer, {
            cors: {
                origin: '*',
                methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH'],
                //   credentials: true,
                //   transports : ['websocket','polling', 'flashsocket'],
            },
            // transports : ['websocket','polling', 'flashsocket'],
        });
    }
    listen(callback) {
        this.httpServer.listen(this.PORT, () => callback(this.PORT));
    }
    handleSocketConnection() {
        this.io.on("connection", (socket) => {
            socket.removeAllListeners();
            socket.removeAllListeners();
            console.log('new WS at=>', socket.id);
            // CREATE-ROOM
            socket.on('create-room', ({ username, room, password, role, editCan, authID, photoURL }) => {
                return this.createRoom(socket, username, room, password, role, editCan, authID, photoURL);
            });
            // JOIN ROOM
            socket.on('join-room', ({ username, room, password, role, authID, photoURL }) => {
                return this.joinRoom(socket, username, room, password, role, authID, photoURL);
            });
            // Listen for sent-points
            socket.on('send-points', ({ action, points, room, authID }) => {
                return this.sendPoints(socket, action, points, room, authID);
            });
            // Send special-points to new joinee
            socket.on('special-points', ({ action, points, to, room, authID }) => {
                return this.specialPoints(socket, action, points, to, room, authID);
            });
            // Send Config to specific users
            socket.on('update-config', ({ roomName, id, editCan }) => {
                this.updateConfig(socket, roomName, id, editCan);
            });
            // same but calling it manually
            socket.on('leave-room', () => {
                console.log('leave room called');
                return this.disconnect(socket);
            });
            // Runs when client disconnects
            socket.on('disconnect', () => {
                return this.disconnect(socket);
            });
            /**
            *  Video call functions
            */
            // socket.on("call-user", (data: any) => {
            //     socket.to(data.to).emit("call-made", {
            //         offer: data.offer,
            //         socket: socket.id
            //     });
            // });
            // socket.on("make-answer", (data: any) => {
            //     socket.to(data.to).emit("answer-made", {
            //         socket: socket.id,
            //         answer: data.answer
            //     });
            // });
            // CHECKING THIS NOW
            socket.on("broadcast", ({ room }) => {
                console.log('on broadcast');
                this.io.to(room).emit("broadcast", {
                    id: socket.id,
                });
            });
            socket.on("watcher", ({ id }) => {
                console.log('on watcher');
                socket.to(id).emit("watcher", {
                    id: socket.id //aka existing room member ID and not newly joined ID
                });
            });
            socket.on("offer", ({ id, offer }) => {
                console.log('on offer');
                socket.to(id).emit("offer", {
                    id: socket.id,
                    offer: offer,
                });
            });
            socket.on("answer", ({ id, answer }) => {
                console.log('on answer');
                socket.to(id).emit("answer", {
                    id: socket.id,
                    answer: answer,
                });
            });
            socket.on("candidate", ({ id, candidate }) => {
                console.log('on candidate');
                socket.to(id).emit("candidate", {
                    id: socket.id,
                    candidate: candidate
                });
            });
        });
    }
    /**
    * Secondary Functions
    */
    createRoom(socket, username, room, password, role, editCan, authID, photoURL) {
        // check if room with same name exist
        var user = (0, server_helper_1.userCreate)(socket.id, username, room, password, role, editCan, authID, photoURL);
        if (user != null) {
            socket.join(user.room);
            // Welcome to current user
            socket.emit('message', {
                type: 'welcome-message',
                message: `Welcome to ${user.room}`,
            });
            socket.emit('create-room-callback', {
                type: "success",
                message: `Welcome to ${user.room}`,
                user: user
            });
            // Send users and room info
            setTimeout(() => {
                this.io.to(user.room).emit('room-users', {
                    room: user.room,
                    users: (0, server_helper_1.getRoomUsers)(user.room)
                });
            }, 5000);
        }
        else {
            // room with same name exist send error
            socket.emit('create-room-callback', {
                type: "error",
                message: `Room with similar name already exist..`,
                user: user
            });
        }
    }
    joinRoom(socket, username, room, password, role, authID, photoURL) {
        var user = (0, server_helper_1.userJoin)(socket.id, username, room, password, role, authID, photoURL);
        if (user && user.error) {
            // Send error to user
            socket.emit('join-room-callback', {
                type: "error",
                message: user.error,
                user: user
            });
            return;
        }
        socket.join(user.room);
        // Welcome to current user
        socket.emit('join-room-callback', {
            type: "success",
            message: `Welcome to ${user.room}`,
            editCan: user.editCan,
            user: user
        });
        // ask creator to emit points
        let userSocketID = (0, server_helper_1.sendAuthorOrJoineeRequest)(user.room);
        if (userSocketID && userSocketID != null) {
            this.io.to(userSocketID).emit('specialCall', {
                type: "send datapoints",
                to: user.id,
                room: user.room
            });
        }
        // BroadCast when a user connects
        socket.broadcast.to(user.room).emit('message', {
            type: 'user-joined-message',
            message: `${user.username} has joined the chat..`,
            id: user.id,
        });
        // Send users and room info
        this.io.to(user.room).emit('room-users', {
            room: user.room,
            users: (0, server_helper_1.getRoomUsers)(user.room)
        });
    }
    sendPoints(socket, action, points, room, authID) {
        let user = (0, server_helper_1.getCurrentUser)(socket.id, room, authID);
        if (!user || user == null)
            return;
        console.log('user made edit =>', user);
        if (user.editCan == true || user.role == "creator") {
            this.io.to(user.room).emit('fetch-points', {
                action: action,
                points: points,
                createdAt: new Date(),
            });
        }
        else {
            console.log(`This user ${user.id} cannot send points due to access restrictions`);
        }
    }
    specialPoints(socket, action, points, to, room, authID) {
        var user = (0, server_helper_1.getCurrentUser)(socket.id, room, authID);
        if (!user || user == null)
            return;
        setTimeout(() => {
            this.io.to(to).emit('fetch-points', {
                action: action,
                points: points,
                createdAt: new Date(),
            });
            // Send users and room info
            this.io.to(room).emit('room-users', {
                room: room,
                users: (0, server_helper_1.getRoomUsers)(user.room)
            });
        }, 5000);
    }
    updateConfig(socket, roomName, id, editCan) {
        // update that room
        let user = (0, server_helper_1.updateUserConfig)(roomName, id, editCan);
        if (user) {
            this.io.to(user.id).emit('config', Object.assign({}, user));
            // Send users and room info
            this.io.to(roomName).emit('room-users', {
                room: user.room,
                users: (0, server_helper_1.getRoomUsers)(user.room)
            });
            socket.emit('update-config-callback', {
                type: "success",
                message: `Configuration updated`,
            });
            return;
        }
        socket.emit('update-config-callback', {
            type: "fail",
            message: `Configuration not updated`,
        });
        return;
    }
    disconnect(socket) {
        console.log('disconnect=>', socket.id);
        var { leftUser, deleteRoom, room } = (0, server_helper_1.userLeave)(socket.id);
        console.log('on disconnect =>', leftUser, deleteRoom, room);
        if (leftUser) {
            // Send users and room info
            this.io.to(leftUser.room).emit('message', {
                type: 'user-left-message',
                message: `${leftUser.username} has left the chat`,
                id: leftUser.id,
            });
            // to remove stream
            this.io.to(leftUser.room).emit("disconnectPeer", socket.id);
            this.io.to(leftUser.room).emit('room-users', {
                room: leftUser.room,
                users: (0, server_helper_1.getRoomUsers)(leftUser.room)
            });
            console.log('deleteRoom=>', deleteRoom);
            if (deleteRoom) {
                socket.leave(room);
                (0, server_helper_1.removeRoom)(room);
            }
        }
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map