"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
class User {
    constructor(obj = {}) {
        this.id = obj.id ? obj.id : "";
        this.username = obj.username ? obj.username : "";
        this.room = obj.room ? obj.room : "";
        this.password = obj.password ? obj.password : "";
        this.role = obj.role ? obj.role : "joinee";
        this.editCan = obj.editCan ? obj.editCan : false;
        this.authID = obj.authID ? obj.authID : "";
        this.photoURL = obj.photoURL ? obj.photoURL : "";
    }
}
exports.User = User;
//# sourceMappingURL=user.model.js.map