"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateUserConfig = exports.removeRoom = exports.sendAuthorOrJoineeRequest = exports.getRoomUsers = exports.userLeave = exports.getCurrentUser = exports.userJoin = exports.userCreate = void 0;
const user_model_1 = require("../model/user.model");
// dictonary of room : array of users { roomName : User[] }
var rooms = {};
// user creates room
function userCreate(id, username, room, password, role, editCan, authID, photoURL) {
    const user = new user_model_1.User({ id, username, room, password, role, editCan, authID, photoURL });
    if (rooms[room] != null)
        return null;
    let userList = [];
    userList.push(user);
    rooms[room] = userList;
    return user;
}
exports.userCreate = userCreate;
// Join user to chat
function userJoin(id, username, room, password, role, authID, photoURL) {
    let joinee = new user_model_1.User({ id, username, room, password, role, authID, photoURL });
    if (rooms[joinee.room] == null)
        return { error: "Room doesn't exists" };
    let roomUsers = rooms[joinee.room];
    if (roomUsers && roomUsers[0]) {
        if (roomUsers[0].password != password) {
            return { error: "Invalid Password" };
        }
        // check if list has authID already
        let flag = roomUsers.findIndex((user) => user.authID == joinee.authID);
        if (flag != -1) {
            return { error: "User has already joined" };
        }
        let creatorIndex = roomUsers.findIndex((user) => user.role == "creator");
        if (creatorIndex == -1) {
            roomUsers[0].role = "creator";
            roomUsers[0].editCan = true;
            rooms[room] = roomUsers;
        }
        // finally get creatorIndex
        creatorIndex = roomUsers.findIndex((user) => user.role == "creator");
        joinee.editCan = roomUsers[creatorIndex].editCan;
        roomUsers.push(joinee);
        rooms[room] = roomUsers;
        return joinee;
    }
    else {
        return { error: "Room has no users" };
    }
}
exports.userJoin = userJoin;
// Get current user
function getCurrentUser(id, room, authID) {
    return rooms && rooms[room] ? rooms[room].find((user) => user.authID == authID) : null;
}
exports.getCurrentUser = getCurrentUser;
// User leaves chat
function userLeave(id) {
    let keyVal;
    let userIndex;
    let userFound = Object.keys(rooms).some((key) => {
        let userList = rooms[key];
        keyVal = key;
        let indee = userList.findIndex((user, index) => {
            userIndex = index;
            return user.id == id;
        });
        return indee == -1 ? false : true;
    });
    if (!userFound)
        return {};
    if (keyVal == null)
        return {};
    if (userIndex == null)
        return {};
    let userList = rooms[keyVal];
    let leftUser = userList.splice(userIndex, 1)[0];
    if (leftUser.role == "creator") {
        let index = userList.findIndex(user => user.role == "creator");
        if (index == -1) {
            // no one in group has edit access or is a creator
            // check if any member exist and give permission
            if (userList && userList[0]) {
                userList[0].editCan = leftUser.editCan;
                userList[0].role = "creator";
            }
        }
    }
    //update userlist
    rooms[keyVal] = userList;
    return {
        leftUser: leftUser,
        deleteRoom: userList.length == 0 ? true : false,
        room: keyVal,
    };
}
exports.userLeave = userLeave;
// Get room users
function getRoomUsers(room) {
    let roomUsers = rooms[room];
    return roomUsers;
    // .map((user: any) => {
    //     return {
    //         id: user.id,
    //         username: user.username,
    //         role: user.role,
    //         editCan: user.editCan,
    //         authID: user.authID,
    //     }
    // })
}
exports.getRoomUsers = getRoomUsers;
function sendAuthorOrJoineeRequest(room) {
    let list = getRoomUsers(room);
    let creatorIndex = list.findIndex((data) => data.role == "creator");
    if (creatorIndex != -1) {
        return list[creatorIndex].id;
    }
    let joineeIndex = list.findIndex((data) => data.role == "joinee");
    if (joineeIndex != -1) {
        return list[joineeIndex].id;
    }
    return null;
}
exports.sendAuthorOrJoineeRequest = sendAuthorOrJoineeRequest;
function removeRoom(room) {
    delete rooms[room];
}
exports.removeRoom = removeRoom;
function updateUserConfig(roomName, id, editCan) {
    let userList = rooms[roomName];
    let userIndex = userList.findIndex((user) => {
        return user.id == id;
    });
    if (userIndex != -1) {
        userList[userIndex].editCan = editCan;
        rooms[roomName] = userList;
        return userList[userIndex];
    }
    return null;
}
exports.updateUserConfig = updateUserConfig;
//# sourceMappingURL=server-helper.js.map